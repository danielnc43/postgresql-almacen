

DROP DATABASE IF EXISTS almacen;
CREATE DATABASE almacen;


CREATE TABLE IF NOT EXISTS public.cajeros (
	cajero serial NOT NULL,
	nomapels varchar(255) NOT NULL,
	CONSTRAINT pk_cajeros PRIMARY KEY (cajero)
);


CREATE TABLE IF NOT EXISTS public.maquinas_registradoras (
	maquina serial NOT NULL,
	piso int4 NOT NULL DEFAULT 1,
	CONSTRAINT pk_maquinas_registradoras PRIMARY KEY (maquina)
);


CREATE TABLE IF NOT EXISTS public.productos (
	producto serial NOT NULL,
	nombre varchar(100) NOT NULL,
	precio money NOT NULL DEFAULT 0,
	CONSTRAINT pk_productos PRIMARY KEY (producto)
);


CREATE TABLE IF NOT EXISTS public.venta (
	venta serial NOT NULL,
	cajero int4 NULL,
	maquina int4 NULL,
	producto int4 NULL,
	CONSTRAINT pk_venta PRIMARY KEY (venta),
	CONSTRAINT venta_cajero_fkey FOREIGN KEY(cajero) REFERENCES cajeros(cajero) ON DELETE NO ACTION,
	CONSTRAINT venta_maquina_fkey FOREIGN KEY(maquina) REFERENCES maquinas_registradoras(maquina) ON DELETE NO ACTION,
	CONSTRAINT venta_producto_fkey FOREIGN KEY(producto) REFERENCES productos(producto) ON DELETE NO ACTION
);

CREATE OR REPLACE FUNCTION public.populate()
RETURNS VOID AS $$
BEGIN  
	
	ALTER TABLE public.venta DROP CONSTRAINT venta_cajero_fkey;
	ALTER TABLE public.venta DROP CONSTRAINT venta_producto_fkey;
	ALTER TABLE public.venta DROP CONSTRAINT venta_maquina_fkey;


  	TRUNCATE TABLE public.productos RESTART IDENTITY;
	DROP SEQUENCE IF EXISTS sec_productos;
	CREATE SEQUENCE sec_productos INCREMENT 1 START 1;
	INSERT INTO public.productos (nombre, precio)
	select
	    CONCAT('Producto ', nextval('sec_productos')) AS producto, (random()*(100-10)+10)::float8::NUMERIC::money AS precio
	from generate_series(1, 50) s(i);


	TRUNCATE TABLE public.cajeros RESTART IDENTITY;
	DROP SEQUENCE IF EXISTS sec_cajeros;
	CREATE SEQUENCE sec_cajeros INCREMENT 1 START 1;
	INSERT INTO public.cajeros (nomapels)
	select
	    CONCAT('Cajero ', nextval('sec_cajeros')) AS cajero
	from generate_series(1, 50) s(i);

	TRUNCATE TABLE public.maquinas_registradoras RESTART IDENTITY;
	DROP SEQUENCE IF EXISTS sec_maquinas_registradoras;
	CREATE SEQUENCE sec_maquinas_registradoras INCREMENT 1 START 1;
	INSERT INTO public.maquinas_registradoras (piso)
	select
	    nextval('sec_maquinas_registradoras') AS piso
	from generate_series(1, 50) s(i);


	TRUNCATE TABLE public.venta RESTART IDENTITY;
	INSERT INTO public.venta
	(cajero, maquina, producto)
	SELECT c.cajero , mr.maquina, p.producto FROM cajeros c, maquinas_registradoras mr, productos p ORDER BY random() LIMIT 5000;


	ALTER TABLE public.venta ADD CONSTRAINT venta_cajero_fkey FOREIGN KEY (cajero) REFERENCES cajeros(cajero);
	ALTER TABLE public.venta ADD CONSTRAINT venta_producto_fkey FOREIGN KEY (producto) REFERENCES productos(producto);
	ALTER TABLE public.venta ADD CONSTRAINT venta_maquina_fkey FOREIGN KEY (maquina) REFERENCES maquinas_registradoras(maquina);

	RAISE NOTICE 'QUERY EXECUTED SUCCESSFULLY';  
    RETURN ;

END $$ LANGUAGE plpgsql;