-- 1. Generar script para poblar todas las tablas.
SELECT populate();

-- 2. Mostrar el número de ventas de cada producto, ordenado de más a menos ventas.
SELECT p.producto , p.nombre, count(1) AS total_ventas
FROM venta v INNER JOIN productos
p ON p.producto = v.producto 
GROUP BY p.producto , p.nombre ORDER BY total_ventas DESC, p.nombre;

-- 3 Obtener un informe completo de ventas, indicando el nombre del cajero que realizo la venta, nombre y precios de los productos vendidos, y el piso en el que se encuentra la máquina registradora donde se realizó la venta.

SELECT c.nomapels AS cajero_nombre, p.nombre AS productoNombre, mr.piso 
FROM venta v INNER JOIN cajeros c ON c.cajero = v.cajero 
INNER JOIN maquinas_registradoras mr ON mr.maquina = v.maquina 
INNER JOIN productos p ON p.producto = v.producto 
ORDER BY c.nomapels, p.nombre , mr.piso 

-- 4. Obtener las ventas totales realizadas en cada piso.
SELECT mr.piso , count(1) AS total_ventas 
FROM venta v INNER JOIN maquinas_registradoras mr ON mr.maquina = v.maquina 
GROUP BY mr.maquina , mr.piso ORDER BY total_ventas DESC, mr.piso ;

--5. Obtener el código y nombre de cada cajero junto con el importe total de sus ventas.
SELECT c.cajero, c.nomapels, sum(p.precio) AS total_monto_venta
FROM venta v INNER JOIN cajeros c ON c.cajero = v.cajero 
INNER JOIN productos p ON p.producto = v.producto 
GROUP BY c.cajero, c.nomapels ORDER BY total_monto_venta DESC, c.cajero 

--6 Obtener el código y nombre de aquellos cajeros que hayan realizado ventas en pisos cuyas ventas totales sean inferiores a los 5000 pesos.
SELECT c.cajero, c.nomapels, sum(p.precio) AS total_monto_venta
FROM venta v INNER JOIN cajeros c ON c.cajero = v.cajero 
INNER JOIN productos p ON p.producto = v.producto 
GROUP BY c.cajero, c.nomapels HAVING (sum(p.precio::NUMERIC) < 5000) ORDER BY total_monto_venta DESC, c.cajero 




